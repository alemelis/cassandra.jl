#=_
 / ) _  _ _ _   _/_ _
(__ (/_)_) (//)(// (/
=#

module Cassandra
    using Printf
    using Crayons

    include("structs.jl")
    include("constants.jl")
    include("cvt.jl")
    include("bitboard.jl")
    include("king.jl")
    include("knight.jl")
    include("rook.jl")
    include("bishop.jl")
    include("magic.jl")
    include("pawn.jl")
    include("move.jl")
    include("perft.jl")

    function greetings()
        return "2. Ke2!?"
    end
end
