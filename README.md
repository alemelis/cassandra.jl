# Cassandra.jl

[![pipeline status](https://gitlab.com/alemelis/cassandra.jl/badges/master/pipeline.svg)](https://gitlab.com/alemelis/cassandra.jl/-/commits/master)
[![Coverage](https://gitlab.com/alemelis/Cassandra.jl/badges/master/coverage.svg)](https://gitlab.com/alemelis/Cassandra.jl/commits/master)

_[Cassandra](https://en.wikipedia.org/wiki/Cassandra), the daughter of King Priam and Queen Hecuba, was given the gift of prophecy but was also cursed by the god Apollo so that her accurate prophecies would not be believed._

![img](cassandra.png)
