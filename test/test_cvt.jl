@testset "cvt" begin
    @printf("\n> cvt\n")
    
    # toInt
    @test Cx.toInt("1") == UInt64(1)
    @test Cx.toInt("10") == UInt64(2)
    @test Cx.toInt("11") == UInt64(3)

    # PGN2UINT
    a8uint = Cx.PGN2UINT["a8"]
    @test Cx.UINT2PGN[a8uint] == "a8"

    # PGN2INT
    @test Cx.PGN2INT["a8"] == 1
    @test Cx.INT2PGN[1] == "a8"

    # INT2UINT
    @test Cx.INT2UINT[1] == Cx.PGN2UINT["a8"]
    uint = Cx.PGN2UINT["a8"]
    @test Cx.UINT2INT[uint] == 1
end
p2u = Cx.PGN2UINT
