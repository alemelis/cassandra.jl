@testset "rook.jl" begin
    @printf("> rook\n")

	@test Cx.orthoSlide(p2u["a1"]) == Cx.MASK_RANKS[1] | Cx.MASK_FILES[1] ⊻ p2u["a1"] ⊻ p2u["a8"] ⊻ p2u["h1"]
    @test Cx.orthoSlide(p2u["h8"]) == Cx.MASK_RANKS[8] | Cx.MASK_FILES[8] ⊻ p2u["h8"] ⊻ p2u["a8"] ⊻ p2u["h1"]
    @test Cx.orthoSlide(p2u["e4"]) == (Cx.MASK_RANKS[4] | Cx.MASK_FILES[5] ⊻ p2u["e4"] ⊻
                                       p2u["e8"] ⊻ p2u["e1"] ⊻ p2u["a4"] ⊻ p2u["h4"])
    @test Cx.ROOK_MASKS[p2u["a1"]] == Cx.MASK_RANKS[1] | Cx.MASK_FILES[1] ⊻ p2u["a1"] ⊻ p2u["a8"] ⊻ p2u["h1"]
    @test Cx.ROOK_MASKS[p2u["h8"]] == Cx.MASK_RANKS[8] | Cx.MASK_FILES[8] ⊻ p2u["h8"] ⊻ p2u["a8"] ⊻ p2u["h1"]
    @test Cx.rookMasksGen()[p2u["e4"]] == (Cx.MASK_RANKS[4] | Cx.MASK_FILES[5] ⊻ p2u["e4"] ⊻
                                           p2u["e8"] ⊻ p2u["e1"] ⊻ p2u["a4"] ⊻ p2u["h4"])

    @test Cx.orthoAttack(p2u["a1"], p2u["a2"]) == Cx.MASK_RANKS[1] ⊻ p2u["a1"] | p2u["a2"]
    @test Cx.orthoAttack(p2u["a1"], Cx.EMPTY) == Cx.MASK_RANKS[1] | Cx.MASK_FILES[1] ⊻ p2u["a1"]
    @test Cx.orthoAttack(p2u["e3"], Cx.EMPTY) == Cx.MASK_RANKS[3] | Cx.MASK_FILES[5] ⊻ p2u["e3"]
end
