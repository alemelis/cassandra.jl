using Cassandra
using Test
using Printf

Cx = Cassandra
@testset "Cassandra.jl" begin
    @test Cx.greetings() == "2. Ke2!?"

    include("test_cvt.jl")
    include("test_bitboard.jl")
    include("test_king.jl")
    include("test_knight.jl")
    include("test_rook.jl")
    include("test_bishop.jl")
    include("test_magic.jl")
    include("test_move.jl")

    @printf("\n")
end
