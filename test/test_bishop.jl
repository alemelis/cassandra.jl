@testset "bishop.jl" begin
    @printf("> bishop\n")

    @test Cx.BISHOP_MASKS[p2u["a1"]] == Cx.BISHOP_MASKS[p2u["h8"]]
    @test Cx.BISHOP_MASKS[p2u["a8"]] == Cx.BISHOP_MASKS[p2u["h1"]]
    @test Cx.diagoSlide(p2u["e3"]) == Cx.BISHOP_MASKS[p2u["e3"]]
    @test Cx.bishopMasksGen()[p2u["d6"]] == reduce(|, [p2u[s] for s = ["b4", "c5", "c7", "e7", "e5", "f4", "g3"]])
end
